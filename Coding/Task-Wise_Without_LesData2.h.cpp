/**
 *  This program is ment to keep track of tasks and help the user with sorting
 *  managing their scedual.
 *  Task-Wise includes:
 *      - Being able to add new tasks with a name, deadline and priority
 *      - The user should be able to mark a task as complete/uncomplete
 *      - Edit the task information in case anything changes or is misspelled
 *      - Delete a chosen task that is no longer needed
 *
 *  @author Ahmad Masoud Mzafar
 *  @author Vegard Berg
 *  @author Christopher Andreas Kindlien
 *  @author Tiril Hvidsand
 */


#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <cctype>
#include <cstdlib>

using namespace std;


const int TOTALTASKS = 400;
const int MAXCHAR = 200;

//******************************************************************************

/**
*   Keeps track of the task priority
*/
enum Priority {High = 1,  Medium = 2,  Low = 3 };

//******************************************************************************

/**
*   class Task with (name, nr, deadline, priority and status(completed)).
*/
class Task {

    private:

    string taskName;
    int day, month, year, taskNr;
    Priority priority;
    bool isComplete;

    public:
    Task (int nr) {taskNr = nr;} //sets the task nr
    Task (ifstream & in, int nr);
    bool completed();
    void editTask();
    int getDead() const;
    string getName() const {return taskName;}
    int getPrio() const {return priority;}
    void readData();
    void setStatus();
    void writeData()const;
    void WriteToFile(ofstream & out) const;
};

//******************************************************************************

void DeleteTask();
void EditTask();
void changeTaskStatus();
char lesChar(const char* t);
int  lesInt(const char* t, const int min, const int max);
void MenuOut();
void NewTask();
void ReadFromFile();
void ShowCompletedTasks();
void ShowUncompletedTasks();
bool sortByDeadline(const Task* d1, const Task* d2);
bool sortByName(const Task* n1, const Task* n2);
bool sortByPriority(const Task* p1, const Task* p2);
void sortList();
void WriteToFile();

//************************************************

vector <Task*> gTasks;   ///< Datastrucure with pointer for all tasks.


//************************************************

/**
 * Main program
 */
int main (){

    char option;
    ReadFromFile();
    MenuOut();
    option = lesChar("\nOption");

    while (option != 'Q'){
        switch (option){
           case 'N':NewTask();              break;
           case 'E':EditTask();             break;
           case 'D':DeleteTask();           break;
           case 'M':changeTaskStatus();  break;
           case 'U':ShowUncompletedTasks(); break;
           case 'C':ShowCompletedTasks();   break;
           case 'S':sortList();             break;
           default :MenuOut();              break;
     }
     option = lesChar("\nOption");
 }
    WriteToFile();
    return 0;
}

//------------------------------------------------------------------------------
//                           CLASS FUNCTIONS
//------------------------------------------------------------------------------

/**
*   Gets tasks data from file
*/
Task::Task(ifstream & in, int nr){
    char prior; //it is a number but if you use number in switch it goes wrong
    int complete;

    nr = taskNr;
    getline(in, taskName);
    in >> day >> month >> year >> prior >> complete;
    switch (prior){
        case '1': priority = High;   break;
        case '2': priority = Medium; break;
        case '3': priority = Low;    break;
    }
    isComplete = (complete == 1);
    in.ignore();
}

 //*****************************************************************************

/**
*   Rerurn complete when the task is completed.
*   @return returns if the task is complete or not
*/

 bool Task::completed() {
    return isComplete;
}

/**
*   Puts the deadline into something easier to sort and returns the deadline
*   @return returns the easier to sort deadline
*/
int Task::getDead() const {
    string d = to_string(day);
    string m = to_string(month);
    string y = to_string(year);

    string dead = y + m + d;

    int deadline = stoi(dead);
    return deadline;
}

//*****************************************************************************

/**
*   edits the chosen task
*/
void Task::editTask() {
    char choice, prior;
    cout << "\n\tPress the number next to the option you want to change\n"
         << "\t1: Edit priority\n"
         << "\t2: Edit deadline\n"
         << "\t3: Edit name\n"
         << "\t4: Edit all\n"
         << "\tQ: Back";
    while(choice != '1' && choice != '2' && choice != '3' &&
          choice != '4' && choice != 'Q')
        choice = lesChar("option: ");   //loops till valid input

    if(choice != 'Q') { //checks if the user wants to go back
        switch (choice) { //change priority
            case '1': cout << "\n\tChange task priority (H)ight, (M)edium, (L)ow\n\tCurrent task priority : ";
                if (priority == High)
                    cout << "High\n";
                if (priority == Medium)
                    cout << "Medium\n";
                if (priority == Low)
                    cout << "Low\n";
                cout << "\tNew priority";
                while (prior != 'H' && prior != 'M' && prior != 'L')
                    prior = lesChar(""); //loops till valid priority
                switch (prior) {         //sets the priority
                case 'H': priority = High; break;
                case 'M': priority = Medium; break;
                case 'L': priority = Low; break;
                } break;

            case '2': cout << "\n\tChanging the deadling\n\tCurrent deadline(DD/MM/YYYY): "
                           << day << "/" << month << "/" << year << '\n';

                cout << "\tEnter the Deadline: (DD/MM/YYYY)\n";
                day = lesInt("\tDay   (--/MM/YYYY)", 1, 31);
                cout << "\tMonth ("; if(day < 10) cout << "0";
                cout << day << "/--/YYYY)";
                month = lesInt("", 1, 12);
                cout << "\tYear  ("; if(day < 10) cout << "0"; cout << day << "/";
                if(month < 10) cout << "0"; cout << month << "/----)";
                year = lesInt("", 2021, 2099);
                break;

            case '3': cout << "\n\tEdit task name\n\tCurrent task name: "
                           << taskName << "\n\tEnter new task name: ";
                getline(cin, taskName);
                break;

            case '4': cout << "\n\tEdit everything\n\tCurrent info:\n\n";
                writeData();
                readData(); //reads the new data
                break;
        }
    }
}

//******************************************************************************

/**
*   Reads the data for the task
*/
void Task::readData(){
    char toto;
    string dato;

    cout << "\tEnter The Task Name: "; getline(cin,taskName);
    cout << "\tEnter the Deadline: (DD/MM/YYYY)\n";
    day = lesInt("\tDay   (--/MM/YYYY)", 1, 31);
    cout << "\tMonth ("; if(day < 10) cout << "0";
    cout << day << "/--/YYYY)";
    month = lesInt("", 1, 12);
    cout << "\tYear  ("; if(day < 10) cout << "0"; cout << day << "/";
    if(month < 10) cout << "0"; cout << month << "/----)";
    year = lesInt("", 2021, 2099);    taskNr++;
    isComplete = false;

    do {
        toto = lesChar("\n\tPriority type [H]igh, [M]edium, [L]ow");
    }while ( toto != 'H' && toto!= 'M' && toto!= 'L' );
    switch (toto){
        case 'H': priority = High;   break;
        case 'M': priority = Medium; break;
        case 'L': priority = Low;    break;
    }
}

//******************************************************************************

/**
 *  Set the task completed/True .
 */

void Task::setStatus() {
    if(!isComplete)
        isComplete = true;
    else
        isComplete = false;
}

//******************************************************************************

/**
*   Writes Task data on the screen
*/

void  Task::writeData()const {
    cout <<"\tTask Name: "<< taskName << "\n\tDeadline(DD/MM/YYYY): ";
    if(day < 10) cout << "0";
    cout << day << "/";
    if(month < 10) cout << "0";
    cout << month << "/" << year << "\n\tPriority: ";
    if(priority == High)
        cout << "High\n";
    if(priority == Medium)
        cout << "Medium\n";
    if(priority == Low)
        cout << "Low\n";
}

//******************************************************************************

 /**
 *  writes the data to file
 */
 void Task::WriteToFile(ofstream & out) const {
    out << taskNr << ' ' << taskName << '\n'  << day << ' ' << month << ' '
        << year << ' ' << priority << ' ' <<(isComplete ? "1" : "0") << '\n';
 }

//------------------------------------------------------------------------------
//                                FUNCTIONS
//------------------------------------------------------------------------------
/**
*   chosen task number will be deleted, and the back-est task in vector will
*   move to the deleted tasks' place.
*   Print all tasks' details and ask the user to take the task number
*   delete, will delete the chosen number
*   @see Task::completed()
*/
void DeleteTask(){
    char sure;
        if(!gTasks.empty()) {
        cout << "\tHere are all Tasks with details:\n\n";

        for (int i = 0 ; i < gTasks.size(); i++) {
            cout <<"\tTask Nr" << setw(2) << i+1 << '\n'; gTasks[i]-> writeData();
            cout << "\n";
        }

        int nr = lesInt ("\t\nChose Task Number To Delete:", 1, gTasks.size());
        cout << "\n\tAre you sure you want to delete this task?\n\n"; gTasks[nr-1]->writeData();
        do {
            sure = lesChar("\n\t(J/N)"); }
        while(sure != 'J' && sure != 'N');
        if(sure == 'J') {
            delete gTasks[nr-1];
            gTasks[nr-1] = gTasks[gTasks.size()-1];
            gTasks.pop_back();
            cout << "\n\tTask has been deleted!\n";
        }
        else cout << "\n\tTask was not deleted!\n";
    }else cout << "There are no tasks saved";
    MenuOut();
}

//******************************************************************************

/**
*   Read task number to for editing
*   @see Task::completed()
*/
void EditTask(){
    if(!gTasks.empty()) {
        cout << "\tInput the number next to the task you would like to edit: \n\n";

        for (int i = 0 ; i < gTasks.size(); i++) {
            cout <<"\tTask Nr" << setw(2) << i+1 << '\n'; gTasks[i]-> writeData();
            cout << "\n";

        }

        int nr = lesInt ("\t\nChose task number to edit:", 1, gTasks.size());
        gTasks[nr-1]->editTask();
        MenuOut();
    }else cout << "\nThere are no tasks to edit!\n";
}

//******************************************************************************

/**
*   Show the list numbers of tasks to be marked as completed/uncomplete.
*   @see Task::completed()
*/
void changeTaskStatus(){
    if(!gTasks.empty()) {
        for (int i = 0 ; i < gTasks.size(); i++) {
            cout <<"\tTask Nr" << setw(2) << i+1 << '\n'; gTasks[i]-> writeData();
            if(!gTasks[i]->completed())
                cout << "\tStatus: Uncomplete\n\n";
            else
                cout << "\tStatus: Complete\n\n";
        }
        int nr = lesInt ("\t\nChose task to be marked as complete/uncomplete:", 1, gTasks.size());
        gTasks[nr-1]->setStatus();
        cout << "\tThe task has been marked as " << (gTasks[nr-1]->completed() ? "complete\n" : "uncomplete\n");

        MenuOut();
    }else cout << "\nThere are no tasks to mark as complete/uncomplete!\n";
}

//******************************************************************************

/**
 *  Reads and returns one (toupper) character.
 *
 *  @param   t  - for user when requesting one character
 *
 *  @return  toupper character.
 */
char lesChar(const char* t)  {
     char tegn;
     std::cout << t << ":  ";
     std::cin >> tegn;  std::cin.ignore(MAXCHAR, '\n');
     return (toupper(tegn));
}

//******************************************************************************

/**
 *  Reads and returns an integer between two given limits.
 *
 *  @param   t    -Command text to the user when requesting input / a number
 *  @param   min  - Minimum for read and accepted numerical value
 *  @param   max  -Maximum for read-in and accepted numerical value
 *
 *  @return Accepted value in the interval 'min' - 'max'
 */
int lesInt(const char* t, const int min, const int max)  {
    char buffer[MAXCHAR] = "";
    int  tall = 0;
    bool feil = false;

    do {
        feil = false;
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        tall = atoi(buffer);
        if (tall == 0 && buffer[0] != '0')
        {  feil = true;   std::cout << "\nERROR: Not an integer\n\n";  }
    } while (feil  ||  tall < min  ||  tall > max);

    return tall;
}

//******************************************************************************

/**
*   Print the menu options on the screen.
*/
void MenuOut(){
    cout
    << "\nTask Wise Choices:\n"
    << "\tN)  Enter new task\n"
    << "\tE)  Edit task\n"
    << "\tD)  Delete Task\n"
    << "\tM)  Mark task as complete/uncomplete\n"
    << "\tU)  Show uncompleted tasks\n"
    << "\tC)  Show completed tasks\n"
    << "\tS)  List sorting\n"
    << "\tQ)  Quit and save Task Wise\n";
}

//******************************************************************************
/**
*   Adds a new tasks to the vector
*/
void NewTask( ){
   int nr;
   char check;
   Task* newTask;   // pointer for new tasks

    nr = gTasks.size(); //saves the start amount of tasks so it displays only the new ones
    do {
        newTask = new Task(gTasks.size());               //save new Task.
        newTask->readData();                              // read the data member
        gTasks.push_back(newTask);                       // put it in vector
        cout << "\n\tTask has been added with task-Nr:"<< gTasks.size() <<  "\n\n";
        do {
            check = lesChar("\tDo you want to add more tasks?(Y/N)");
        }while(check != 'N' && check != 'Y');
    } while(check != 'N');
    cout << "\tThe new tasks has been added\n\n";

    for(int i = nr; i < gTasks.size(); i++) {           // print the new task(s) added:
        gTasks[i]->writeData();
        cout << "\n";
    }
    cout << "\n\tTotal numbers of tasks has been added now are:" << gTasks.size()<< "\n\n";

    MenuOut();
}

//******************************************************************************

/**
*   Reads data from file
*   @see Tast::Task(...)
*/
void ReadFromFile() {
    ifstream  in("task.DTA");                 //  pner aktuell fil
    Task* newTask;
    int nr;

    if (in) {                                   //  Filen funnet:
        cout << "\n\nReading from file task.DTA ...\n\n";
        in >> nr;
        while(!in.eof()){
            newTask = new Task(in, nr);
            gTasks.push_back(newTask);
            in >> nr;
        }
        in.close();      //lukker filen
    } else
      cout << "\n\nCouldnt fine the file task.DTA!\n\n"; //hvis filen ikke finnes
}

//******************************************************************************

/**
*   Function to show all completed tasks
*   @see MenuOut()
*/
void ShowCompletedTasks() {
    int i;

    i = count_if(gTasks.begin(), gTasks.end(), [](const auto & val) {return (val->completed());});
    if(!gTasks.empty()) {
        if(i > 0) {
            for (int i = 0; i < gTasks.size();i++) {
                if (gTasks[i]->completed()) {
                    gTasks[i]->writeData();
                    cout << "\n";
                }
            }
            MenuOut();
        }else cout << "There are no completed tasks\n";
    }else cout << "There are no tasks added\n";
}

//******************************************************************************

/**
*   Function to show all uncompleted tasks.
*   @see MenuOut()
*/
void ShowUncompletedTasks() {
    int i;

    i = count_if(gTasks.begin(), gTasks.end(), [](const auto & val) {return (!val->completed());});
    if(!gTasks.empty()) {
        if(i > 0) {
            for (int i = 0; i < gTasks.size(); i++) {
                if (!gTasks[i]->completed()) {
                    gTasks[i]->writeData();
                    cout << "\n";
                }
            }
            MenuOut();
        }else cout << "\nThere are no uncompleted tasks\n";
    }else cout << "\nThere are no tasks added\n";
}

//******************************************************************************

/**
*   Sorts the vector by deadline
*   @see Task::getDead()
*   @return returns the values to be sorted
*/
bool sortByDeadline(const Task* d1, const Task* d2) {
        return (d1->getDead() < d2->getDead());
}

//******************************************************************************

/**
*   Sorts the vector by name
*   @see Task::getName()
*   @return returns the values to be sorted
*/
bool sortByName(const Task* n1, const Task* n2) {
    return (n1->getName() < n2->getName());
}

//******************************************************************************

/**
*   Sorts the vector by priority
*   @see Task::getPrio()
*   @return returns the values to be sorted
*/
bool sortByPriority(const Task* p1, const Task* p2) {
    return (p1->getPrio() < p2->getPrio());
}

//******************************************************************************

/**
*   Sorts the tasks
*   @see Task::getName()
*   @see Task::getDead()
*   @see Task::getPrio()
*   @see MenuOut()
*/
void sortList() {
    char choice;
    if(!gTasks.empty()) {
        cout << "\tList sorting:\n"
             << "\tN) Sort by name\n"
             << "\tD) Sort by deadline\n"
             << "\tP) Sort by priority\n";
        while(choice != 'N' && choice != 'D' && choice != 'P')
        choice = lesChar("Chose how you want to sort the list");

        switch(choice) {
            case 'N': sort(gTasks.begin(), gTasks.end(), sortByName);
                cout << "\tTasks are now sorted by name!\n";
                break;
            case 'D': sort(gTasks.begin(), gTasks.end(), sortByDeadline);
                cout << "\tTasks are now sorted by deadline!\n";
                break;
            case 'P': sort(gTasks.begin(), gTasks.end(), sortByPriority);
                cout << "\tTasks are now sorted by priority!\n";
                break;
        }
        MenuOut();
    }else cout << "\nThere are no tasks to edit!\n";
}

//******************************************************************************

/**
*   Writes all the saved tasks to file
*   @see Task::WriteToFile(...)
*/
void WriteToFile(){
    ofstream out("task.DTA");   //finner filen

    if(out) { //hvis filen finnes
        cout << "Writing data to file...";
        for(int i = 0; i < gTasks.size(); i++)
            gTasks[i]->WriteToFile(out);
    }
    else        //hvis filen ikke finnes
        cout <<"Could not find the file to write data!\n";
}



/**
 *  For reading: characters and numbers 
 *
 *   @file     LesData2.H
 *   
 */


#ifndef __LESDATA2_H
#define __LESDATA2_H


#include <iostream>            //  cin, cout
#include <iomanip>             //  setprecision
#include <cctype>              //  toupper
#include <cstdlib>             //  atoi, atof

const int  MAXCHAR = 200;      // Max characters in input buffer. 

char  lesChar(const char* t);
float lesFloat(const char* t, const float min, const float max);
int   lesInt(const char* t, const int min, const int max);


/**
 *  Reads and returns one (toupper) character. 
 *
 *  @param   t  - for user when requesting one character
 *
 *  @return  toupper character.
 */
 char lesChar(const char* t)  {
     char tegn;
     std::cout << t << ":  ";
     std::cin >> tegn;  std::cin.ignore(MAXCHAR, '\n');
     return (toupper(tegn));
}


/**
 *  Reads and returns a floating point number between two given limits
 *
 *  @param   t    - Command text to the user when requesting input / a number 
 *  @param   min  - Minimum for read and accepted numerical value
 *  @param   max  - Maximum for read-in and accepted numerical value
 *
 *  @return Accepted value in the interval 'min' - 'max' 
 */
float lesFloat (const char* t, const float min, const float max)  {
    char buffer[MAXCHAR] = "";
    float tall = 0.0F;
    bool  feil = false;

    do {
        feil = false;
        std::cout << std::fixed << std::showpoint << std::setprecision(2);
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        tall = static_cast <float> (atof(buffer));
        if (tall == 0 && buffer[0] != '0')
           {  feil = true;   std::cout << "\nERROR: Not a float\n\n";  }
    } while (feil ||  tall < min  ||  tall > max);

    return tall;
}


/**
 *  Reads and returns an integer between two given limits.
 *
 *  @param   t    -Command text to the user when requesting input / a number 
 *  @param   min  - Minimum for read and accepted numerical value
 *  @param   max  -Maximum for read-in and accepted numerical value
 *
 *  @return Accepted value in the interval 'min' - 'max'
 */
int lesInt(const char* t, const int min, const int max)  {
    char buffer[MAXCHAR] = "";
    int  tall = 0;
    bool feil = false;

    do {
        feil = false;
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        tall = atoi(buffer);
        if (tall == 0 && buffer[0] != '0')
        {  feil = true;   std::cout << "\nERROR: Not an integer\n\n";  }
    } while (feil  ||  tall < min  ||  tall > max);

    return tall;
}

#endif
